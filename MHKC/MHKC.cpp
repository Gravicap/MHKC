﻿// MHKC.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include "ProcSeq.h"

using namespace std;

//----------------------------------------------------------------------------

ostream & operator << (ostream & os, const TSequence & rSeq);

void PrintLOGO(void);

//----------------------------------------------------------------------------

int main()
{
	PrintLOGO();

	TSequence seq = {
		{ 1, 10 }, { 2, 11 }, { 3, 11 }, { 4, 11 },
		{ 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 },
		{ 9, 11 }, { 10, 11 }, { 11, 10 }
	};

	cout << "Before call ProcessSequence()" << endl
		<< seq << endl;

	ProcessSequence(seq, 3);

	cout << "After call ProcessSequence() with n = 3" << endl
		<< seq << endl;

	seq = {
		{ 1, 10 }, { 2, 11 }, { 3, 11 }, { 4, 11 },
		{ 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 },
		{ 9, 11 }, { 10, 11 }, { 11, 10 }
	};

	cout << "Before call ProcessSequence()" << endl
		<< seq << endl;

	ProcessSequence(seq, 4);

	cout << "After call ProcessSequence() with n = 4" << endl
		<< seq << endl;

}

//----------------------------------------------------------------------------

ostream & operator << (ostream & os, const TSequence & rSeq)
{
	for (const auto & elem : rSeq) {
		cout << '{' << elem.first << ',' << elem.second << '}';
	}

	return os << endl;
}

void PrintLOGO(void)
{
	cout
		<< "                  "
		<< "+------------------------------------------+" << endl
		<< "                  "
		<< "|  Welcome to the MHKC Test Task program!  |" << endl
		<< "                  "
		<< "+------------------------------------------+" << endl
		<< endl << endl;
}
