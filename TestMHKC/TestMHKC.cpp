﻿// TestMHKC.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "..\MHKC\ProcSeq.h"


TEST(ProcessSequence_wrong, n_eq_3)
{
	TSequence sample{ {1, 10}, { 2, 11 }, { 3, 11 }, { 4, 11 }, { 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 }, { 9, 11 }, { 10, 11 }, { 11, 10 } };
	TSequence seq_with_n_eq_3{ {1, 10}, {2, 11}, {4, 11}, {5, 11}, {6, 10}, {7, 11}, {9, 11}, {10, 11}, {11, 10} };

	ProcessSequence_wrong(sample, 3);
	EXPECT_EQ(sample, seq_with_n_eq_3);
}

TEST(ProcessSequence_wrong, n_eq_4)
{
	TSequence sample{ {1, 10}, { 2, 11 }, { 3, 11 }, { 4, 11 }, { 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 }, { 9, 11 }, { 10, 11 }, { 11, 10 } };
	TSequence seq_with_n_eq_4{{1, 10}, {2, 11}, {5, 11}, {6, 10}, {7, 11}, {10, 11}, {11, 10}};

	ProcessSequence_wrong(sample, 4);
	EXPECT_EQ( sample, seq_with_n_eq_4 );
}

TEST(ProcessSequence_wrong, n_eq_100)
{
	TSequence sample = { {1, 50}, {2, 50}, {3, 50}, {4, 60}, {5, 60}, {6, 70} };
	TSequence seq_with_n_eq_100{ {1, 50}, {3, 50}, {4, 60}, {5, 60}, {6, 70} };

	ProcessSequence_wrong(sample, 100);
	EXPECT_EQ(sample, seq_with_n_eq_100);
}

TEST(ProcessSequence_wrong, n_eq_5)
{
	TSequence sample = { {1, 50}, {2, 50}, {3, 50}, {4, 60}, {5, 60}, {6, 60}, {7, 60}, {8, 70} };
	TSequence seq_with_n_eq_5{ {1, 50}, {3, 50}, {4, 60}, {5, 60}, {7, 60}, {8, 70} };

	ProcessSequence_wrong(sample, 5);
	EXPECT_EQ(sample, seq_with_n_eq_5);
}

TEST(ProcessSequence, n_eq_3)
{
	TSequence sample{ {1, 10}, { 2, 11 }, { 3, 11 }, { 4, 11 }, { 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 }, { 9, 11 }, { 10, 11 }, { 11, 10 } };
	TSequence seq_with_n_eq_3{ {1, 10}, {2, 11}, {4, 11}, {5, 11}, {6, 10}, {7, 11}, {9, 11}, {10, 11}, {11, 10} };

	ProcessSequence(sample, 3);
	EXPECT_EQ(sample, seq_with_n_eq_3);
}

TEST(ProcessSequence, n_eq_4)
{
	TSequence sample{ {1, 10}, { 2, 11 }, { 3, 11 }, { 4, 11 }, { 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 }, { 9, 11 }, { 10, 11 }, { 11, 10 } };
	TSequence seq_with_n_eq_4{ {1, 10}, {2, 11}, {5, 11}, {6, 10}, {7, 11}, {10, 11}, {11, 10} };

	ProcessSequence(sample, 4);
	EXPECT_EQ(sample, seq_with_n_eq_4);
}

TEST(ProcessSequence, n_eq_2)
{
	TSequence sample{ {1, 10}, { 2, 11 }, { 3, 11 }, { 4, 11 }, { 5, 11 }, { 6, 10 }, { 7, 11 }, { 8, 11 }, { 9, 11 }, { 10, 11 }, { 11, 11 }, { 12, 11 }, { 13, 11 }, { 14, 11 }, { 15, 12 } };
	TSequence seq_with_n_eq_2{ {1, 10}, {2, 11}, {5, 11}, {6, 10}, {7, 11}, {14, 11}, {15, 12} };

	ProcessSequence(sample, 2);
	EXPECT_EQ(sample, seq_with_n_eq_2);
}

