#include <algorithm>
#include "ProcSeq.h"

void ProcessSequence_wrong(TSequence & rSeq, size_t nEveryNth)
{
	if (rSeq.size() > 2 && nEveryNth > 2) {
		auto itCur = rSeq.cbegin();
		TSequence result = { *itCur++ };
		for (TSequence::size_type n = 1; itCur != rSeq.cend(); ++n, ++itCur) {
			if (result.crbegin()->second != itCur->second) {
				result.insert(*itCur);
			}
			else {
				if (n % nEveryNth != 0) {
					auto it = itCur;
					while (++it != rSeq.cend()
						&& it->second == itCur->second
						&& ++n % nEveryNth != 0
					) {
						++itCur;
					}
					result.insert(*itCur);
				}
				else {
					result.insert(*itCur);
				}
			}
		}
		rSeq.swap(result);
	}
}

void ProcessSequence(TSequence & rSeq, size_t nEveryNth)
{
	if (rSeq.size() > 2) {
		auto itCur = rSeq.cbegin();
		TSequence resSeq = { *itCur };
		while (++itCur != rSeq.cend()) {
			if (resSeq.crbegin()->second != itCur->second) {
				resSeq.insert(*itCur);
			}
			else {
				auto itBound = std::prev( std::find_if(itCur, rSeq.cend(),
					[itCur](auto el) { return el.second != itCur->second; }
				) );
				if (nEveryNth > 2) {
					for(TSequence::size_type n = 2; itCur != itBound; ++n, ++itCur ) {
						if (0 == n % nEveryNth) {
							resSeq.insert(*itCur);
						}
					}
				}
				else {
					itCur = itBound;
				}
				resSeq.insert(*itCur);
			}
		}
		rSeq.swap(resSeq);
	}
}
